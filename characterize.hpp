#ifndef __CHARACTERIZE__
#define __CHARACTERIZE__

#define PREC 4096

char* characterize(char* num) {
        mpfr_t nz;
        mpfr_init(nz);
        mpfr_set_str(nz, num, 10, MPFR_RNDD);
        mpfr_t lognum;
        mpfr_init(lognum);
        mpfr_log(lognum, nz, MPFR_RNDD);
        mpfr_t pif;
        mpfr_init(pif);
        char* pistr = new char[PREC+1];
        strncpy(pistr, pi, PREC);
        pistr[PREC] = '\0';
        mpfr_set_str(pif, pistr, 10, MPFR_RNDD);
        mpfr_log(pif, pif, MPFR_RNDD);
        mpfr_div(lognum, lognum, pif, MPFR_RNDD);
        char* pivot =new char[PREC+1];
        mpfr_exp_t expptr;
        mpfr_get_str(pivot, &expptr, 10, 0, lognum, MPFR_RNDD);
        cout << pivot << endl;
        return pivot;
}
#endif
