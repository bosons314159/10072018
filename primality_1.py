#!/usr/local/bin/python
import sys
from flask import Flask,request,jsonify
import operator
from primes import primes
from pi import pi
from e import e
import gmpy2

app = Flask(__name__)

def convert(x, y):
   ss = ""
   while  x > 0:
       d = x % y
       ss = ss  + str(d)
       x /= y
   ss = ss[::-1]
   return int(ss)

def analyzeforPowersOf2(z, base):
   lz = list(str(z))
   prod = 1
   cover = False
   for x in range(0, 17):
       if sorted(lz) == sorted(list(str(prod))):
           cover = True
           break
       prod = prod*2
   return cover

def primality_test_helper(num):
   l = len(num)
   cnt = 0
   dn_arr = []
   while cnt < l:
      pk = int(pi[cnt])
      ek = int(e[cnt])
      nk = int(num[cnt])
      cn = pk*100 + nk*10 + ek
      dn_arr.append(cn)
      cnt = cnt + 1
   hit = 0
   for x in dn_arr:
     present = analyzeforPowersOf2(x, x)
     if present == True:
       continue
     present  = False
     for y in range(9, 2, -1):
          z = convert(x, y)
          present = analyzeforPowersOf2(z, x)
          if present:
              break
     if present == False:
          hit = hit + 1
   return hit

def primality_test(num):
   ss= str(num)
   hit = 5
   cnt = 1
   while hit > 0:
        hit = primality_test_helper(ss)
        if hit == 0:
           break
        return hit 
   return True
        
@app.route('/', methods=['GET','POST'])
def factorize():
   num = str(request.args['num'])
   isPrime = primality_test(num)
   return jsonify(isPrime)
