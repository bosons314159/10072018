#!/usr/local/bin/python

import sys
from pi import pi
from e import e

def find(pat, offset):
     cnt = 0
     idx =0
     while cnt < offset: 
        idx = pi.index(pat, idx)
        cnt = cnt + 1
        if cnt == offset:
           print(idx)
           print(e[int(str(idx))])
           return idx, e[idx]
        else:
           idx = idx+1

if __name__ == "__main__":
    pat = str(sys.argv[1])
    offset = int(sys.argv[2])
    find(pat, offset)
