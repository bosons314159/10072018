#!/usr/local/bin/tcsh

foreach x ( `seq 1 1 25`) 
    (gtime --verbose ./factorize `cat testcases/man$x.txt` > stdout) &> ./man$x.csv
end

foreach x ( `glob testcases/rsa*.txt`  ) 
    (gtime --verbose ./factorize `cat testcases/$x.txt` > stdout) &> ./$x.csv
end

(gtime --verbose ./factorize `cat testcases/latest.txt` > stdout)  &> ./manindra.csv

(gtime --verbose ./factorize `cat testcases/fml.txt` > stdout) &> ./fml.csv
