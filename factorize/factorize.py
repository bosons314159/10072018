#!/usr/local/bin/python

import sys
pi = "3141592653"
e = "2718281828"

def rotateL(pp, x):
     pp = pp[x:] + pp[:x]
     return pp

def rotateR(pp, x):
     pp = pp[-x:] + pp[:-x]
     return pp

if __name__ == "__main__":
     num = str(sys.argv[1])
     rnum = num[::-1]
     zz = list(zip(num, rnum))
     rr = []
     for k in zz:
         s = "".join(k)
         s = int(s)
         rr.append(s)
     rr = (rr)
     c = 0
     l = len(num)
     pp = pi
     ee = e[::-1]
     zo = (list(zip(pp, ee)))
     cnt = 0
     pr = -1
     while c  < 99:
        r = (rr[c % l])
        if r > pr:
		pp = rotateL(pp, int(r / 10))
		ee = rotateR(ee, int(r % 10)) 
        elif e < pr:
		pp = rotateR(pp, int(r / 10))
		ee = rotateL(ee, int(r % 10)) 
        else:
                pass
        zk = (list(zip(pp, ee)))
        k = (len(list(set(sorted(zo))-set(sorted(zk)))))
        if (k % 10 == 0):
		print("Iteration : " + str(cnt + 1))
        cnt = cnt + 1
        c = c + 1
