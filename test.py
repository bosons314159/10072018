#!/usr/local/bin/python

import sys
from pi import *
from e import *
from primes import *

if __name__ == "__main__":
    pp  = pi[:int(sys.argv[2])]
    ee  = e[:int(sys.argv[2])][::-1]
    num = str(sys.argv[1])
    lnn = len(num)
    cnt = 0
    l = int(sys.argv[2])
    res = []
    while cnt < int(sys.argv[2]):
         cn = pp[cnt] + num[cnt % lnn] + ee[cnt]
         cn_ = cn[::-1]
         if int(cn) in primes and int(cn_) in primes:
              res.append(l-1-cnt)
              break
         cnt = cnt + 1
    print(len(res)>0)
