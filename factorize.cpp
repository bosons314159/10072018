#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <string>
#include <string.h>
#include <algorithm>
#include <vector>
#include <boost/lexical_cast.hpp>
#include "pi.hpp"
#include "e.hpp"
#include "primes.hpp"

using namespace boost;
using namespace std;

template<typename T>
void print(vector<T> v) {
	for (int i =0; i < v.size(); ++i) {
		cout << v[i] << " , ";
	}
	cout << endl;
}

char* convert(char* num, int base) {
	mpz_t nz;
	mpz_init(nz);
	mpz_set_str(nz, num, 10);
	mpz_t rm;
	mpz_init(rm);
	std::string ss = "";
	while (mpz_cmp_si(nz, 0) > 0) {
		mpz_mod_ui(rm, nz, 2);
		mpz_div_ui(nz, nz, 2);
		ss = ss + strdup(mpz_get_str(0, 10, rm));
	}
	std::reverse(ss.begin(), ss.end());
	mpz_clear(nz);
	mpz_clear(rm);
	return strdup(ss.c_str());
}

bool check(char* num, unsigned long long int ctr, const char* pi,const char* e) {
	unsigned long long int cnt = ctr;
	mpz_t pacc;
	mpz_init(pacc);
	mpz_set_si(pacc, 0);
	mpz_t eacc;
	mpz_init(eacc);
	mpz_set_si(eacc, 0);
	mpz_t nz;
	mpz_init(nz);
	mpz_set_str(nz, num, 10);;
	while (1) {
		int pk = pi[cnt]  - '0';
		int ek = e[cnt] - '0';
		mpz_add_ui(pacc, pacc, pk);
		mpz_add_ui(eacc, eacc, ek);
		if (mpz_cmp(pacc, nz) > 0 || mpz_cmp(eacc, nz) > 0) {
			mpz_clear(pacc);
			mpz_clear(eacc);
			mpz_clear(nz);
			return false;
		}
		char* tp = mpz_get_str(0, 10, pacc);
		char* te = mpz_get_str(0, 10, eacc);
		if (strstr(num, tp) && strstr(num, te) && strcmp(tp, te)==0) {
			int cn = pk*10 + ek;
			int cn_ = ek*10 + pk;
			vector<int>::iterator it = std::find(primes.begin(), primes.end(), cn);
			vector<int>::iterator it_ = std::find(primes.begin(), primes.end(), cn_);
			if (it != primes.end() || it_ != primes.end() ) {
				mpz_clear(pacc);
				mpz_clear(eacc);
				mpz_clear(nz);
				return true;
			} else {
				mpz_clear(pacc);
				mpz_clear(eacc);
				mpz_clear(nz);
				return false;
			}
		}
		++cnt;
	} 
	mpz_clear(pacc);
	mpz_clear(eacc);
	mpz_clear(nz);
	return false;
}

int main(int argc, char* argv[]) {
	char* num = strdup(argv[1]);
	cout << "Number entered was : " << num << endl;
        char* bnum = convert(num, 2);
        cout << bnum << endl;
	unsigned long long int ctr = 0;
	int hit = 200;
	int h = 0;
	char tail = 0;
	std::string logfactor = "";
	bool first = true;
	while (1) {
		if (first || (pi[ctr] ==tail) || (e[ctr] == tail)) {
			bool succ = check(num, ctr, pi, e);
			if (succ) {
				++h;
				if (first) {
					first = false;
					logfactor += pi[ctr];
					logfactor += e[ctr];
					tail = e[ctr];
				} else if ((pi[ctr] == tail) || (e[ctr] == tail)) {
					if (pi[ctr] == tail) {
						tail = e[ctr];
					} else  {
						tail = pi[ctr];
					}
					logfactor += tail;
				}
				if (h >= hit) break;
				cout << logfactor << endl;
				cin.get();
			}
		}
		++ctr;          
	}
	return 0;
}
