#!/usr/local/bin/python

import sys
from primes import primes

if __name__ == "__main__":
    nruns = int(sys.argv[1])
    f = open("/Volumes/INTFACT/intfact/tpl.txt","w")
    nprimes = []
    nnums = []
    nums = []
    g= 0
    t= 0
    for x in range(1, nruns):
            g = g + 1
	    c = 1
            h = 0
            succ = False
	    while h < x:
	       nnums.append(c % 10)
               t = t + 1
               if t >= nruns:
                 succ = True
                 break
	       c = c + 1
               h = h + 1
	       if c % 10 == 0:
		   c = c + 1
               if h <= (x-1):
                   nums.append(" ")
            if succ == True:
               break
            if g % 10 == 0:
                g= g + 1
                nums.append(str(g % 10)) 
            else:
                nums.append(str(g % 10)) 
    t = 0
    for  x in range(0, nruns):
       if primes[x] > 0:
          nprimes.append("1")
          t  = t + 1
          if t >= nruns:
             break
       succ = False
       for y in range(primes[x]+1, primes[x+1]):
            nprimes.append("*")
            t = t + 1
            if t >= nruns:
                succ = True
                break
       if succ == True:
           break
    cols = 80
    start = 0
    for x in range(0, int(nruns/80)):
        for y in range(start, start+80):
            f.write(str(nprimes[y])+",")
        f.write("\n")
        for y in range(start, start+80):
            f.write(str(nnums[y])+",")
        f.write("\n")
        for y in range(start, start+80):
            f.write(str(nums[y])+",")
        f.write("\n")
        f.write("\n")
        f.write("\n")
        f.write("\n")
        f.write("\n")
        start = start + 80
        if (start + 80 ) >= nruns:
            break
    rem  = nruns - int(nruns/80)
    for y in range(0, rem):
        f.write(str(nprimes[y])+",")
    f.write("\n")
    for y in range(0, rem):
        f.write(str(nnums[y])+",")
    f.write("\n")
    for y in range(0, rem):
        f.write(str(nums[y])+",")
    f.write("\n")
    f.write("\n")
    f.write("\n")
    f.close()
