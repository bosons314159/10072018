#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <string>
#include <string.h>
#include <algorithm>
#include <vector>
#include <boost/lexical_cast.hpp>
#include "pi.hpp"
#include "e.hpp"
#include "primes.hpp"

using namespace boost;
using namespace std;

#define PREC 10

template<typename T>
void print(vector<T> v) {
	for (int i =0; i < v.size(); ++i) {
		cout << v[i] << " , ";
	}
	cout << endl;
}

int main(int argc, char* argv[]) {
	char* num = strdup(argv[1]);
	bool found = false;
	int idx2 = 0;
	int l = strlen(num);
	std::string numstr = "";
	l = strlen(num);
	cout << "Number entered was : " << num << endl;
	vector<long long int> posets;
	char ch = 0;
	FILE* hp = fopen("/Volumes/INTFACT/intfact/nums.dat","r");
	long long int pos = 0;
	posets.push_back(-1);
	for (int times= 0; times < PREC; ++times) {
		for (int i = 0; i < l; ++i) {
			int nk = num[i]- '0';
			while (nk == 0) {
				while ((ch=fgetc(hp)) != '9') {
					++pos;
				}
                                ++pos;
				while ((ch=fgetc(hp)) != '1') {
					++pos;
				}
                                ++pos;
				posets.push_back(pos);
				++i;
				nk  = num[i] - '0';
			}
			while ((ch=fgetc(hp)) != num[i]) { 
				++pos;
			}
                        ++pos;
			posets.push_back(pos);
		}
		posets.push_back(-1);
	}         
	fclose(hp);
	print(posets);
	FILE* fp = fopen("/Volumes/INTFACT/intfact/nprimes.dat","r");
	char gh = 0;
	vector<int> logf;
	int hit = 0;
	int state = -1, start = 0, stop = 0;
	for (int i = 0; i < posets.size()-1;++i) {
		if (posets[i] == -1 && state < 1) {
			state = 1;
			continue;
		}
		if (state == 1 && posets[i] != -1)  {
			start = posets[i];
			state = 2;
			continue;
		}
		if (state == 2 && posets[i+1] == -1) {
			stop = posets[i];
			state = 0;
			fseek(fp, start, SEEK_SET);
			int idx = start;
			while (idx <= stop+1)  {
				gh = fgetc(fp);
                                ++idx;
				if (idx == stop && gh == '1') {
					++hit;
					logf.push_back(hit % 10);
				cout << idx << "\t"<<gh<<endl;
				}
				if (gh == '1') ++hit;
			}
			//calculate number of primes between start and stop
			hit = 0;
			continue;
		}     
	}         
	print(logf);
	cout << endl << endl;
	fclose(fp);
}
