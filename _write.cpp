#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <string>
#include <string.h>
#include <algorithm>
#include <vector>
#include <boost/lexical_cast.hpp>
#include "pi.hpp"
#include "e.hpp"
#include "primes.hpp"

using namespace boost;
using namespace std;

#define NRUNS 1000000

template<typename T>
void print(vector<T> v) {
	for (int i =0; i < v.size(); ++i) {
		cout << v[i] << " , ";
	}
	cout << endl;
}

int main(int argc, char* argv[]) {
	FILE* fp = fopen("/Volumes/INTFACT/intfact/nprimes.dat","w");
	FILE* hp = fopen("/Volumes/INTFACT/intfact/nums.dat","w");
	FILE* gp = fopen("/Volumes/INTFACT/intfact/nnums.dat","w");
	unsigned long long int g = 0;
	unsigned long long int t = 0;
	unsigned long long int t2 = 0;
	for (int x = 1; x < NRUNS; ++x) {
		++g;
		unsigned int c = 1;
		unsigned int h = 0;
		bool succ = false;
		while (h < x) {
			fprintf(gp, "%u", c % 10);
			++t2;
			++c;
			++h;
			if (c % 10 == 0) ++c;
			if (h <= (x - 1)) {
				fprintf(hp, "*");
				++t;
			}
			if (t >= NRUNS) {
				succ = true;
				break;
			}
		}
		if (succ) break;
		if (g % 10 == 0) {
			++g;
			fprintf(hp, "%llu",g % 10);
			++t;
		} else {
			fprintf(hp, "%llu",g % 10);
			++t;
		}
	} 
	t = 0;
	for (int x = 0; x < NRUNS; ++x) {
		if (primes[x] > 0) {
			fprintf(fp, "1");
			++t;
			if (t >= NRUNS) break;
		}
		bool succ = false;
		for (unsigned long long int y = primes[x] + 1;y < primes[x+1]; ++y) {
                        if (y % 10 == 0) continue;
			fprintf(fp, "*");
			++t;
			if (t >= NRUNS) {
				succ = true;
				break;
			}
		}
		if (succ) break;
	} 
	fclose(fp);
	fclose(hp);
	fclose(gp);
}
