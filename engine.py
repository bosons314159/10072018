#!/usr/local/bin/python

import sys
from micro import strategy
from pi import pi
from e import e
from zeros import zeros
from primes import primes


if __name__ == "__main__":
    num = str(sys.argv[1])
    l = len(num)
    cnt = 0
    ctr = 0
    while True:
       res = []
       pcnt = 0
       ecnt = 0
       match1 = False
       match2 = False
       _pi = pi[:zeros[ctr]]
       _e = e[:zeros[ctr]][::-1]
       cnt = 0
       h = 0
       while cnt < zeros[ctr]-1:
          pp = _pi[cnt]+_pi[cnt+1]
          ee = _e[cnt] + _e[cnt+1]
          match1 = not strategy(num, int(pp))
          match2 = not strategy(num, int(ee))
          if match1 == True:
             pcnt = pcnt + 1
          if match2 == True:
             ecnt = ecnt + 1
          res.append([match1, match2])
          if match1 == True and match2 == True:
             h = h + 1
          cnt = cnt + 1
          if cnt >= zeros[ctr]-1:
              break
       print(num[ctr], primes[ctr], zeros[ctr],h)
       ctr = ctr + 1
