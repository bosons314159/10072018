#!/usr/local/bin/tcsh

foreach x ( `seq 1 1 25`) 
    gtime  ./factorize `cat testcases/man$x.txt` > man$x.csv
end

set list=`ls -1 ../testcases/rsa*.txt`
set cnt = 0
foreach x ( `sed 's/\.\.\/testcases\///' $list` )
    gtime  ./factorize $x > rsa$cnt.csv
    set cnt = `expr $cnt + 1`
end

gtime  ./factorize `cat testcases/latest.txt` > manindra.csv

gtime  ./factorize `cat testcases/fml.txt` > fml.csv
