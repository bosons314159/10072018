#!/usr/local/bin/python

import sys
from pi import pi
from e import e
from primes import primes

def convert(x, b):
      ss = ""
      while x > 0:
          ss = ss + str(x % b)
          x = int(x / b)
      return ss[::-1]

if __name__ == "__main__":
     num = str(sys.argv[1])
     num = num*10000
     sumx = 0
     factor = []
     k = 0
     iter = 0
     for n in num:
       nk = int(n)
       sumx = sumx + nk
       pp = pi[:sumx]
       ee = e[:sumx][::-1]
       ll = []
       mm = []
       for z in list(zip(pp,ee)):
             cn = z[0] + z[1]
             if int(cn) in primes or int(cn[::-1]) in primes:
                 ll.append(cn)
       for z in list(zip(pp,ee[::-1])):
             cn = z[0] + z[1]
             if int(cn) in primes or int(cn[::-1]) in primes:
                 mm.append(cn)
       l = len(ll)
       h = len(mm)
       if l == h:
            iter = iter + 1
       if l == h and (l in primes): ## or int(str(l)[::-1]) in primes):
            print("pass" + " " + str(iter))
       k = k + 1
