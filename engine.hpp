#ifndef __ENGINE__
#define __ENGINE__
#include "zeros.hpp"
#include <stdio.h>

char* engine(char* pivot) {
	FILE* fp = fopen("./dump.txt", "w");
	int j = 0;
	std::string factor = "";
	int pos = 0, neg = 0;
	vector<int> pvec;
	vector<int> nvec;
	for (int i = 0; i < PREC; ++i) {
		int pk = pivot[i] - '0';
		if (j % 10 == 0) { ++j;}
		for (int m = primes[i] ; m < primes[i+1];++m) {
			if (m % 10 == 0) continue;
			if (pk == 0) {
				if (m == primes[i] ) {
					pos = 0;
					neg = 0;
					pvec.push_back(pos);
					nvec.push_back(neg);
					continue;
				}
				int cnt1  = 0;
				for (int u  = primes[i]; u < m; ++u){ 
					if (u % 10 == 0 ) continue;
					++cnt1;
				}
				neg = cnt1;
				int cnt2 = 0;
				for (int u  = m; u < primes[i+1]; ++u){ 
					if (u % 10 == 0 ) continue;
					++cnt2;
				}
				pos = cnt2;
				pvec.push_back(pos);
				nvec.push_back(neg);
			}
		}
		++j;
	}
	int nprimes = 0, nzeros = 0;
	for (int i = 0; i < pvec.size()-1; ++i) {
		bool s1 = false;
		if (pvec[i]==nvec[i])  {
			++nprimes;
			s1 = true;
			fprintf(fp, "X\t,");
		} else {
			fprintf(fp, "*\t,");
		}
		int cn1 = pvec[i]*10 + pvec[i+1];
		int cn2 = nvec[i]*10 + nvec[i+1];
		int _cn1 = pvec[i+1]*10 + pvec[i];
		int _cn2 = nvec[i+1]*10 + nvec[i];
		vector<int>::iterator it1 = std::find(zeros.begin(), zeros.end(), cn1);
		vector<int>::iterator it2 = std::find(zeros.begin(), zeros.end(), cn2);
		vector<int>::iterator it3 = std::find(zeros.begin(), zeros.end(), _cn1);
		vector<int>::iterator it4 = std::find(zeros.begin(), zeros.end(), _cn2);
		bool s2 =false;
		if (it1 != zeros.end() || it2 != zeros.end() || it3 != zeros.end() || it4 != zeros.end()) {
			++nzeros;
			s2 = true;
			fprintf(fp, "R\t,");
		} else {
			fprintf(fp, "*\t,");
		}
		fprintf(fp, "%d\t%d\n,", pvec[i], nvec[i]);
		if (s1 && s2 ) {
			fprintf(fp, ">>>> %d,\t%d\n\n",nprimes, nzeros) ;
			nzeros = 0;
			nprimes = 0;
		}
	}
	fclose(fp);
}

#endif
