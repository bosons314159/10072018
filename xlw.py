#!/usr/local/bin/python

import xlsxwriter
from pi import pi
from e import e

workbook = xlsxwriter.Workbook("./tmpl.xlsx")
worksheet = workbook.add_worksheet()
row = 1
col = 0
while col < 10000:
    worksheet.write(row, col, pi[col]+pi[col+1])
    col = col + 1

col = 0
row = 5
while col < 10000:
    worksheet.write(row, col, e[col]+e[col+1])
    col = col + 1
   
workbook.close()
