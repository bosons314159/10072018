#!/usr/local/bin/python

import sys
from pi import *
from e import *
from primes import *

def strategy(num, sz):
    pp  = pi[:sz]
    ee  = e[:sz][::-1]
    lnn = len(num)
    cnt = 0
    l = int(sz)
    res = []
    while cnt < int(sz):
         cn = pp[cnt] + num[cnt % lnn] + ee[cnt]
         cn_ = cn[::-1]
         if int(cn) in primes and int(cn_) in primes:
              res.append(l-1-cnt)
              break
         cnt = cnt + 1
    return (len(res)>0)
