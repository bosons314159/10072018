#!/usr/local/bin/python

f = open("./primes1.txt", "r")
f2 = open("./_primes.txt", "w") 

f2.write("[ ")
c = "1"
while c:
   c = f.read(1)
   if c:
      if c in ['0','1','2','3','4','5','6','7','8','9']:
         f2.write(c)
      else:
         while c in [' ', '\t', '\n']:
             c = f.read(1)
         f2.write(", ")
         if c in ['0','1','2','3','4','5','6','7','8','9']:
             f2.write(c)
f2.write(" ]\n")
f.close()
f2.close()

