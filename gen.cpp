#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <string>
#include <string.h>
#include <algorithm>
#include <vector>
#include <boost/lexical_cast.hpp>
#include "pi.hpp"
#include "e.hpp"
#include "primes.hpp"

using namespace boost;
using namespace std;

int main(int argc, char* argv[]) {
	mpz_t prod;
	mpz_init(prod);
	mpz_set_si(prod, 2);
	const char* ul = "10000";
	mpz_t limit;
	mpz_init(limit);
	mpz_set_str(limit, ul, 10);
	mpz_t cnt;
	mpz_init(cnt);
	mpz_set_si(cnt, 0);
	FILE* fp = fopen("/Volumes/INTFACT/intfact//power2.txt", "w");
	while (mpz_cmp(cnt, limit) <= 0) {
		char* pri = mpz_get_str(0, 10, prod);
		fprintf(fp, "%s", pri);
		mpz_mul_ui(prod, prod, 2);
		mpz_add_ui(cnt, cnt, 1);
	}
	fclose(fp);
}
